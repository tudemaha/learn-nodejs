const db = require('./db_config');

// cek koneksi
// db.connect((err) => {
//     if(err) throw err;
//     console.log('DB connected');
// });

// buat database
// const create = 'CREATE DATABASE tudemaha';
// db.query(create, (err, result) => {
//     if(err) throw err;
//     console.log('Berhasil menambahkan database');
// });

// membuat tabel
// const crateTable = `CREATE TABLE mahasiswa(
//     id int NOT NULL,
//     name VARCHAR(255),
//     email VARCHAR(255),
//     PRIMARY KEY(id)
// )`;
// db.query(crateTable, (err, result) => {
//     if(err) throw err;
//     console.log('Tabel berhasil dibuat.');
// });

// insert one data
// const insert = `INSERT INTO mahasiswa VALUES (2108561055, 'Mahardika', 'mahardika@contoh.com')`;
// db.query(insert, (err, result) => {
//     if(err) throw err;
//     console.log('Berhasil menambah data mahasiswa');
// });

// insert more than one data
// const insertMany = `INSERT INTO mahasiswa (id, name) VALUES ?`;
// const values = [
//     [2108673433, 'Anto'],
//     [2107635400, 'Budi'],
//     [8776349332, 'Kadek']
// ];
// db.query(insertMany, [values], (err, result) => {
//     if(err) throw err;
//     console.log('Berhasil menambahkan ' + result.affectedRows);
// });

// read data
// const read = `SELECT * FROM mahasiswa`;
// db.query(read, (err, result) => {
//     if(err) throw err;
//     console.table(result);
// });

// update data
// const update = `UPDATE mahasiswa SET name='Putu' WHERE name='Anto'`;
// db.query(update, (err, result) => {
//     if(err) throw err;
//     console.log('Berhasil mengubah ' + result.affectedRows + ' data');
// });

// delete data
// const deleteData = `DELETE FROM mahasiswa WHERE id=2108561055`;
// db.query(deleteData, (err, result) => {
    // console.log('Berhasil menghapus ' + result.affectedRows + ' data');
    // console.log(result);
// });