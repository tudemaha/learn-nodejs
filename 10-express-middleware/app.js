const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const morgan = require('morgan');

const app = express();
const port = 3000;

// app.engine('ejs', () => {});
app.set('view engine', 'ejs');

// third party middleware
// use expressLayout to make template
app.use(expressLayouts);
app.use(morgan('dev'));

const mahasiswa = [
    {
        nama: 'Tude Maha',
        email: 'tudemaha@contoh.com'
    },
    {
        nama: 'Dony',
        email: 'dony@gmail.com'
    },
    {
        nama: 'Yanto',
        email: 'yantokomang@yahoo.com'
    }
];

// built-in middleware
app.use(express.static('public'));

// application level middleware
app.use((req, res, next) => {
    console.log('Time: ', Date.now());
    next(); // gunakan next agar menuju ke middleware selanjutnya, tidak hang
});


// get tidak perlu next karena setelah ketemu, dijalankan (html dirender), dan proses selesai
// jika ditambahkan akan menghasilkan error jika tidak ada kondisi next
app.get('/', (req, res) => {
    res.render('index', {
        name: 'Tude Maha',
        title: 'Halaman Home',
        mahasiswa,
        layout: './layouts/main-layout'
    });
});

app.get('/contact', (req, res) => {
    res.render('contact', {
        title: 'Halaman Contact',
        layout: './layouts/main-layout'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        layout: './layouts/main-layout',
        title: 'Halaman About'
    });
});

app.use('/', (req, res) => {
    res.status(404);
    res.send('<h1>Error: 404 Not Found</h1>');
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});