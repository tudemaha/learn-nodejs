const {MongoClient, ObjectId} = require('mongodb');

const uri = 'mongodb://127.0.0.1:27017';
const client = new MongoClient(uri);

const dbName = 'test';


client.connect((error, client) => {
    if(error) {
        return console.log('Koneksi gagal');
    }
    
    // console.log('Koneksi berhasil');
    
    const db = client.db(dbName);
    
    // insert one document
    // db.collection('mahasiswa').insertOne(
    //     {
    //         nama: 'Yanto',
    //         umur: 20
    //     },
    // (error, result) => {
    //     if(error) return console.log('ada error nih');
    //     console.log(result);
    // });
        
        
    // insert more than one document
    // db.collection('mahasiswa').insertMany(
    //     [
    //         {nama: 'Ari', umur: 19},
    //         {nama: 'Yusuf', umur: 20}
    //     ],
    //     (error, result) => {
    //         if(error) return console.log('ada error');
    //         console.log(result);
    // });
                    
    // show all documents
    // console.log(db
    //     .collection('mahasiswa')
    //     .find()
    //     .toArray((error, result) => {
    //         console.log(result);
    //     })
    // );
                        
    // menampilkan documen berdasarkan kriteria
    // console.log(db
    //     .collection('mahasiswa')
    //     .find({_id: ObjectId("62c8a6993ddf915ccf5ad92b")})
    //     .toArray((error, result) => {
    //         console.log(result);
    //     })
    // );

    // update one document
    // const update = db.collection('mahasiswa').updateOne(
    //     {
    //         _id: ObjectId("62c8a42ad3ea1a0a0c082179")
    //     },
    //     {
    //         $set: {
    //             umur: 18
    //         }
    //     }
    // );

    // update
    //     .then((result) => console.log(result))
    //     .catch((err) => console.log(err));

    // mengubah data lebih dari satu
    // db.collection('mahasiswa').updateMany(
    //     {
    //         nama: 'Yusuf'
    //     },
    //     {
    //         $set: {
    //             nama: 'Yanto'
    //         }
    //     }
    // )

    // menghapus satu data
    // db.collection('mahasiswa').deleteOne(
    //     {
    //         _id:  ObjectId("62c8a36f4794ab522f9a0d7a")
    //     }
    // )
    //     .then((result) => console.log(result))
    //     .catch((err) => console.log(err));

    // menghapus lebih dari satu data
    db.collection('mahasiswa').deleteMany(
        {nama: 'Yanto'}
    )
        .then((result) => console.log(result))
        .catch((err) => console.log(err));
});