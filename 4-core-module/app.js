const fs = require('fs');
const { normalize } = require('path');
const readline = require('readline');

/*
menulis file synchronous: writeFileSync();
menulis file asynchronous: writeFile();
*/

// menulis secara synchronous
// menuliskan file jika tidak ada file, menimpa jika ada
// fs.writeFileSync("sync.txt", "menulis file secara synchoronous");

// tidak bisa digunakan untuk membuat directory jika tidak teredia
// gunakan block try catch untuk menangkap error
// try {
//     fs.writeFileSync("./data/data.txt", "menulis file secara synchronous");
// } catch(e) {
//     console.log(e);
// }

// menulis file asynchronous
// fs.writeFile("./data/data.txt", 'menulis file secara asynchronous', err => {
//     if(err) throw err;
//     console.log("Berhasil menulis file secara synchronous");
// });


// membaca isi file (sync)
// console.log(fs.readFileSync("sync.txt", 'utf-8'));

// membaca isi file(async)
// fs.readFile('async.txt', 'utf-8', (err, data) => {
//     if(err) throw err;
//     console.log(data);
// });


// readline
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// oneline input
// rl.question('Halo, apa kabar? ', answer => {
//     console.log(`Oke, kabarmu ${answer}`);

//     rl.close();
// });

// multiline input
rl.question('Nama: ', nama => {
    rl.question('Nomor: ', nomor => {
        console.log(`Hai, nama saya ${nama}, nomor HP ${nomor}`);
        rl.close(); //pindah rl.close() di bagian paling dalam
    });
});