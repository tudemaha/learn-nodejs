const fs = require('fs');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Nama\t: ', nameData => {
    rl.question('Nomor\t: ', numberData => {
        const contactData = {
            nama: nameData,
            nomor: numberData
        };

        const contacts = JSON.parse(fs.readFileSync('./contacts.json', 'utf-8'));
        contacts.push(contactData);
        fs.writeFileSync('./contacts.json', JSON.stringify(contacts));
        console.log(contacts);

        rl.close();
    })
});