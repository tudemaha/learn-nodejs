const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    // res.json({
    //     nama: 'Tude',
    //     nomor: 55,
    //     email: 'contoh@contoh.com'
    // });
    res.sendFile('./index.html', {root: __dirname});
});

app.get('/about', (req, res) => {
    // res.send('Halaman About');
    res.sendFile('./about.html', {root: __dirname});
});

app.get('/contact', (req, res) => {
    // res.send('Halaman Contact');
    res.sendFile('./contact.html', {root: __dirname});
});

app.get('/product/:id', (req, res) => {
    res.send(`ID produk: ${req.params.id} <br> Kategori: ${req.query.category}`);
});

app.use('/', (req, res) => {
    res.status(404);
    res.send('<h2>404 Not Found</h2>');
});

app.listen(port, () => {
    console.log(`Port is listening on port ${port}`);
});


// const http = require('http');
// const fs = require('fs');

// const renderHTML = (path, res) => {
//     fs.readFile(path, (err, data) => {
//         if(err) {
//             res.writeHead(404);
//             res.write('Error: File not found...');
//         } else {
//             res.write(data);
//         }

//         res.end();
//     });
// }

// http
//     .createServer((req, res) => {
        
//         res.writeHead(200, {
//             'Content-Type': 'text/html'
//         });
        
//         const url = req.url;

//         switch(url) {
//             case '/about':
//                 renderHTML('./about.html', res);
//                 break;
//             case '/contact':
//                 renderHTML('./contact.html', res);
//                 break;
//             default:
//                 renderHTML('./index.html', res);
//                 break;
//         }
//     })
//     .listen(3000, () => {
//         console.log('Server is listening on port 3000...');
//     });