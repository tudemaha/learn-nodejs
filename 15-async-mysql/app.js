const mysql = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'tracktube'
});

const parseData = () => {
    const promise = new Promise((resolve, reject) => {
        db.query('SELECT * FROM users', (error, result) => {
            resolve(result);
            reject(error);
        });
    });
    return promise
        .then(result => JSON.stringify(result))
        .then(result => JSON.parse(result));
}

(async () => {
    const data = await parseData();
    console.log(data);
})();