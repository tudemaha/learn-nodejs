function haloDunia() {
    return 'Hello World!';
}

const PI = 3.14;

const mahasiswa = {
    nama: "Tude Maha",
    umur: 19,
    introduction() {
        return `Halo, saya ${this.nama}, berumur ${this.umur} tahun.`;
    }
}

class Orang {
    constructor() {
        console.log('Object orang telah dibuat.');
    }
}

// cara export pertama (bentuknya object untuk ekspor beberapa bagian)
// module.exports.haloDunia = haloDunia;
// module.exports.PI = PI;
// module.exports.mahasiswa = mahasiswa;
// module.exports.Orang = Orang;

// cara langsung buatkan objectnya
module.exports = {
    haloDunia: haloDunia,
    PI: PI,
    mahasiswa: mahasiswa,
    Orang: Orang
}

// cara lebih simple dengan ES6 (nama properties/method) yang sama dengan valuenya bisa ditulis sekali
module.exports = {haloDunia, PI, mahasiswa, Orang};