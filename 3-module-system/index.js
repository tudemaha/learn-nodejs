/*
urutan js mengenali modul:
- core modules (modul punya node)
- local modules (modul buatan user)
- third party modules (modul yang sudah dibuat orang lain atau npm modules)
    terletak di folder node_modules
*/

// require cuma 1 kali, dimana nama variabel disesuaikan dengan nama module (pada umumnya)
// const fs = require("fs"); // core module
const coba = require("./coba"); // bentuknya object

console.log(coba.haloDunia(), coba.PI, coba.mahasiswa.nama, coba.mahasiswa.introduction(), new coba.Orang());