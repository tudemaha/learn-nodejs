// modules
const express = require('express');
const expressLayout = require('express-ejs-layouts');
const flash = require('connect-flash');
const session = require('express-session');
const {body, check, validationResult} = require('express-validator');
const validator = require('validator');
const methodOverride = require('method-override');

// connect db
require('./utils/db');
const Contact = require('./models/contact')

// express config
const app = express();
const port = 3000;

// view engine config
app.set('view engine', 'ejs');
app.use(expressLayout);
app.use(express.urlencoded({extended: true}));

// flash config
app.use(flash());

// session config
app.use(session({
    cookie: {maxAge: 6000},
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

// method-override config
app.use(methodOverride('_method'));

// homepage
app.get('/', (req, res) => {
    res.render('index', {
        layout: 'layouts/main-layout',
        title: 'Tude Maha Contact App'
    })
});

// contacts
app.get('/contacts', async (req, res) => {
    const contacts = await Contact.find();
    res.render('contacts', {
        layout: './layouts/main-layout',
        title: 'Daftar Kontak',
        msg: req.flash('msg'),
        contacts
    });
});

// add new contact
app.get('/contacts/add', (req, res) => {
    res.render('add-contact', {
        title: 'Tambah Kontak',
        layout: './layouts/main-layout'
    })
});

// save new contact
app.post('/contact', [
    body('name').custom(async (value) => {
        const duplicateContact = await Contact.findOne({name: value});
        if(duplicateContact) {
            throw new Error('Nama kontak sudah digunakan!');
        }

        return true;
    }),
    body('email').custom((value) => {
        if(value.length !== 0 && !validator.isEmail(value)) {
            throw new Error('Email tidak valid!');
        }

        return true;
    }),
    check('number', 'Nomor HP tidak valid!').isMobilePhone('id-ID'),
], (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()) {
            // return res.status(400).json({errors: errors.array()});
            res.render('add-contact', {
                layout: './layouts/main-layout',
                title: 'Tambah Kontak',
                errors: errors.array()
            });
        } else {
            Contact.insertMany(req.body, (error, result) => {
                req.flash('msg', 'Kontak berhasil ditambahkan.');
                res.redirect('/contacts');
            });
        }
});

// contact detail
app.get('/contacts/:name', async (req, res) => {
    const detail = await Contact.findOne({name: req.params.name});
    res.render('detail', {
        layout: './layouts/main-layout',
        title: `Detail Kontak ${req.params.name}`,
        detail
    });
});

// delete contact
app.delete('/contacts', (req, res) => {
    Contact.deleteOne({name: req.body.name}).then((result) => {
        req.flash('msg', 'Kontak berhasil dihapus.');
        res.redirect('/contacts');
    });
});

// edit contact
app.get('/contacts/edit/:name', async (req, res) => {
    const editContact = await Contact.findOne({name: req.params.name});
    res.render('edit-contact', {
        title: 'Edit Kontak',
        layout: './layouts/main-layout',
        contact: editContact
    });
});

// save edited contact
app.put('/contacts', [
    body('name').custom( async (value, {req}) => {
        const duplicateContact = await Contact.findOne({name: req.body.name});
        if(value !== req.body.oldName && duplicateContact) {
            throw new Error('Nama kontak sudah digunakan!');
        }
        return true;
    }),
    body('email').custom((value) => {
        if(value.length !== 0 && !validator.isEmail(value)) {
            throw new Error('Email tidak valid!');
        }

        return true;
    }),
    check('number', 'Nomor HP tidak valid!').isMobilePhone('id-ID')
], (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.render('edit-contact', {
            title: 'Edit Kontak',
            layout: './layouts/main-layout',
            errors: errors.array(),
            contact: req.body
        });
    } else {
        Contact.updateOne({name: req.body.oldName}, {
            $set: {
                name: req.body.name,
                number: req.body.number,
                email: req.body.email
            }
        }).then((result) => {
            req.flash('msg', 'Kontak berhasil diubah.');
            res.redirect('/contacts');
        });
    }
});


// not found
app.use('/', (req, res) => {
    res.render('not-found', {
        layout: './layouts/blank-layout',
        title: 'Halaman Tidak Tersedia'
    });
});

// server listen
app.listen(port, () => {
    console.log(`Mongo Contact App is listening on port https://localhost:${port}`);
});
