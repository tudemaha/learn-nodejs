const sayHello = (nama, umur) => {
    return `Hi, nama saya ${nama}, berumur ${umur} tahun.`;
}

module.exports = sayHello;