// jQuery version
// $('document').ready(() => {
//     $('#edit-form').on('submit', (event) => {
//         event.preventDefault();

//         let value = $('#text').val();
        
//         $.ajax({
//             url: '/update',
//             method: 'POST',
//             contentType: 'application/json',
//             data: JSON.stringify({value}),

//             success: (res) => {
//                 $('#title').html(res.response);
//             }
//         });
//     });
// });

// vanilla JS
const form = document.forms['edit-form'];
const title = document.getElementById('title');
const input = document.getElementById('text');

form.addEventListener('submit', (event) => {
    event.preventDefault();

    console.log(input.value);
    fetch('/update', {
        method: 'POST',
        body: JSON.stringify({value: input.value}),
        headers: {'Content-type': 'application/json; charset=UTF-8'} // bagian penting untuk menyatakan bahwa kita akan mengirim json
    }).then((res) => {
        // console.log(res);
        if(res.ok) {    // ceh responnya dlu, baru gunakan method json()
            return res.json();
        }
        return Promise.reject(res);
    }).then((res) => {
        console.log(res);   // saat then kedua baru dapat hasilnya
        title.innerHTML = res.response;
    }).catch((error) => {
        console.log(error);
    })
});
