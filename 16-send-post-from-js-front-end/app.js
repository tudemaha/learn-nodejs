const { render } = require('ejs');
const express = require('express');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static('public', ({root: __dirname})));
app.use(express.urlencoded({extended: false}));
app.use(express.json()); // ini yang penting saat mengirim object json

app.get('/', (req, res) => {
    res.render('index');
});

app.post('/update', (req, res) => {
    console.log(req.body);
    res.send({response: req.body.value});
})

app.listen(port, () => {
    console.log('testing');
})