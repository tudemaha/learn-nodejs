const fs = require('fs');

const dirPath = './data';
if(!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
}

const contactsPath = './data/contacts.json';
if(!fs.existsSync(contactsPath)) {
    fs.writeFileSync(contactsPath, '[]');
}

const loadContacts = () => {
    return JSON.parse(fs.readFileSync(contactsPath, 'utf-8'));
};

const findContact = (name) => {
    const contacts = loadContacts();
    return contacts.find((contact) => contact.name.toLowerCase() === name.toLowerCase());
}

const saveContacts = (contacts) => {
    fs.writeFileSync(contactsPath, JSON.stringify(contacts));
}

const addContact = (contact) => {
    const contacts = loadContacts();
    contacts.push(contact);
    saveContacts(contacts);
}

const duplicateCheck = (name) => {
    const contacts = loadContacts();
    return contacts.find(contact => contact.name.toLowerCase() === name.toLowerCase());
}

const deleteContact = (name) => {
    const contacts = loadContacts();
    const index = contacts.findIndex(contact => contact.name.toLowerCase() === name.toLowerCase());
    contacts.splice(index, 1);
    saveContacts(contacts);
}

const updateContacts = (oldContact) => {
    const contacts = loadContacts();
    const index = contacts.findIndex(contact => contact.name.toLowerCase() === oldContact.oldName.toLowerCase());
    delete oldContact.oldName;
    contacts.splice(index, 1, oldContact);
    saveContacts(contacts);
}


module.exports = {loadContacts, findContact, addContact, duplicateCheck, deleteContact, updateContacts};