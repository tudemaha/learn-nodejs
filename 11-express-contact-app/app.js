const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const contact = require('./utils/contact');
const {body, validationResult, check} = require('express-validator');
const session = require('express-session');
const flash = require('connect-flash');

const app = express();
const port = 3000;

// middleware
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(express.urlencoded({extended: true}))

// flash configuration
app.use(session({
    cookie: {maxAge: 6000},
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(flash());

app.get('/', (req, res) => {
    res.render('index', {
        title: 'Tude Maha Contact App',
        layout: 'layouts/main-layout'
    });
});

app.get('/contacts', (req, res) => {
    const contacts = contact.loadContacts();
    res.render('contacts', {
        title: 'Daftar Kontak',
        layout: 'layouts/main-layout',
        contacts,
        msg: req.flash('msg')
    })
});

app.get('/contacts/add', (req, res) => {
    res.render('add-contact', {
        title: 'Tambah Kontak',
        layout: './layouts/main-layout'
    })
});

app.post('/contact', [
    body('name').custom(value => {
        const duplicateContact = contact.duplicateCheck(value);
        if(duplicateContact) {
            throw new Error('Nama kontak sudah digunakan!');
        }

        return true;
    }),
    body('email').custom(value => {
        if(value.length !== 0) {
            check('email').isEmail().withMessage('Email tidak valid!');
        }
        return true;
    }),
    check('number', 'Nomor HP tidak valid!').isMobilePhone('id-ID'), // cara 1
    // check('email').isEmail().withMessage('Email tidak valid!')       // cara 2
], (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()) {
            // return res.status(400).json({errors: errors.array()});
            res.render('add-contact', {
                layout: './layouts/main-layout',
                title: 'Tambah Kontak',
                errors: errors.array()
            });
        } else {
            contact.addContact(req.body);
            req.flash('msg', 'Kontak berhasil ditambahkan.');
            res.redirect('/contacts');
        }
});

app.get('/contacts/delete/:name', (req, res) => {
    const deletedContact = contact.findContact(req.params.name);
    if(deletedContact) {
        contact.deleteContact(req.params.name);
        req.flash('msg', 'Kontak berhasil dihapus');
        res.redirect('/contacts');
    } else {
        res.render('not-found', {
            layout: './layouts/blank-layout',
            title: 'Halaman Tidak Tersedia'
        });
    }

});

app.get('/contacts/edit/:name', (req, res) => {
    const editContact = contact.findContact(req.params.name);
    res.render('edit-contact', {
        title: 'Edit Kontak',
        layout: './layouts/main-layout',
        contact: editContact
    });
});

app.post('/contact/update', [
    body('name').custom((value, {req}) => {
        const duplicateContact = contact.duplicateCheck(value);
        if(value !== req.body.oldName && duplicateContact) {
            throw new Error('Nama kontak sudah digunakan!');
        }
        return true;
    }),
    body('email').custom(value => {
        if(value.length !== 0) {
            check('email', 'Email tidak valid!').isEmail();
        }
        return true;
    }),
    check('number', 'Nomor HP tidak valid!').isMobilePhone('id-ID')
], (req, res) => {
    const errors = validationResult(req);
    // console.log(req.body);
    if(!errors.isEmpty()) {
        res.render('edit-contact', {
            layout: './layouts/main-layout',
            errors: errors.array(),
            contact: req.body
        });
    } else {
        contact.updateContacts(req.body);
        req.flash('msg', 'Kontak berhasil diubah.');
        res.redirect('/contacts');
    }
});

app.get('/contacts/:name', (req, res) => {
    const detail = contact.findContact(req.params.name);
    res.render('detail', {
        detail,
        layout: './layouts/main-layout',
        title: `Detail Kontak ${contact.name}`
    })
});


app.use('/', (req, res) => {
    res.status(404);
    res.render('not-found', {
        layout: 'layouts/blank-layout',
        title: 'Halaman Tidak Tersedia'
    });
});

app.listen(port, () => {
    console.log(`Contact app listening on port ${port}`);
});