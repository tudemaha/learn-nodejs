// const contact = require('./contact');
const yargs = require('yargs');
const contact = require('./contact');

// add a new contact to list
yargs.command({
    command: 'add',
    describe: 'add a new contact',
    builder: {
        name: {
            describe: 'full name',
            demandOption: true,
            type: 'string'
        },
        phone: {
            describe: 'phone number',
            demandOption: true,
            type: 'string'
        },
        email: {
            describe: 'email',
            demandOption: false,
            type: 'string'
        }
    },
    handler(argv) {
        contact.saveContact(argv.name, argv.phone, argv.email)
    }
}).demandCommand();

// show all saved contacts
yargs.command({
    command: 'list',
    describe: 'show name and phone number from saved contacts',
    handler() {
        contact.showContacts(); 
    }
});

// show detailed information from a contact based on name
yargs.command({
    command: 'detail',
    describe: 'show detailed information from a contact',
    builder: {
        name: {
            describe: 'full name',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        contact.showDetails(argv.name);
    }
});

yargs.command({
    command: 'delete',
    describe: 'delete contact based on name',
    builder: {
        name: {
            describe: 'full name',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        contact.deleteContact(argv.name);
    }
});
yargs.parse();
