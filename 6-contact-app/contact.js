const fs = require('fs');
const chalk = require('chalk');
const validator = require('validator');
const { resourceLimits } = require('worker_threads');

// create data folder if not exist
const dirPath = './data';
if(!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
}

// crate contacts.json if not exist
const contactPath = './data/contacts.json';
if(!fs.existsSync(contactPath)) {
    fs.writeFileSync(contactPath, '[]', 'utf-8');
}

const loadContacts = () => {
    return JSON.parse(fs.readFileSync('./data/contacts.json', 'utf-8'));
}

// save contact from given values
const saveContact = (name, number, email) => {
    const contact = {name, number, email};
    const contacts = loadContacts();
    
    const duplicate = contacts.find(contact => contact.name === name);
    if(duplicate) {
        console.log(chalk.red.bold('Kontak sudah terdaftar, gunakan nama lain!'));
        return false;
    }

    if(email && !validator.isEmail(email)) {
        console.log(chalk.red.bold('Email tidak valid'));
        return false;
    }

    if(!validator.isMobilePhone(number, 'id-ID')) {
        console.log(chalk.red.bold('Nomor HP tidak valid!'));
        return false;
    }
    
    contacts.push(contact);
    fs.writeFileSync('./data/contacts.json', JSON.stringify(contacts));

    console.log(chalk.bgBlue.bold('Terima kasih telah memasukkan data.'));
}


const showContacts = () => {
    const contacts = loadContacts();
    console.log(chalk.bgGreen.bold('Daftar Kontak'));
    contacts.forEach((contact, i) => {
        console.log(chalk.blue(`Kontak ke-${i + 1}:`));
        console.log(`Nama: ${contact.name}\nNomor HP: ${contact.number}\n`);
    });
}

const showDetails = (name) => {
    const contacts = loadContacts();
    const result = contacts.find(contact => contact.name.toLowerCase() === name.toLowerCase());

    if(result) {
        console.log(chalk.blue.bold(result.name));
        console.log(result.number);
        if(result.email) console.log(result.email);
    } else {
        console.log(chalk.red.bold(`Tidak ada kontak bernama ${name}`));
        return false;
    }
}

const deleteContact = (name) => {
    const contacts = loadContacts();
    const deletedIndex = contacts.findIndex(contact => contact.name.toLowerCase() === name.toLowerCase());

    if(deletedIndex === -1) {
        console.log(chalk.red.bold(`Tidak ada kontak bernama ${name}`));
        return false;
    }

    contacts.splice(deletedIndex, 1);
    fs.writeFileSync('./data/contacts.json', JSON.stringify(contacts));
    console.log(`${name} dihapus.`);
}

module.exports = {saveContact, showContacts, showDetails, deleteContact};