const express = require('express');
const expressLayouts = require('express-ejs-layouts');

const app = express();
const port = 3000;

// app.engine('ejs', () => {});
app.set('view engine', 'ejs');

// use expressLayout to make template
app.use(expressLayouts);

const mahasiswa = [
    {
        nama: 'Tude Maha',
        email: 'tudemaha@contoh.com'
    },
    {
        nama: 'Dony',
        email: 'dony@gmail.com'
    },
    {
        nama: 'Yanto',
        email: 'yantokomang@yahoo.com'
    }
];

app.get('/', (req, res) => {
    res.render('index', {
        name: 'Tude Maha',
        title: 'Halaman Home',
        mahasiswa,
        layout: './layouts/main-layout'
    });
});

app.get('/contact', (req, res) => {
    res.render('contact', {
        title: 'Halaman Contact',
        layout: './layouts/main-layout'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        layout: './layouts/main-layout',
        title: 'Halaman About'
    });
});

app.use('/', (req, res) => {
    res.status(404);
    res.send('<h1>Error: 404 Not Found</h1>');
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});