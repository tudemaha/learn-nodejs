const validator = require('validator');
const chalk = require('chalk');

// validator
console.log(validator.isEmail('aku@you.com'));
console.log(validator.isMobilePhone('08515435665762', 'id-ID'));
console.log(validator.isNumeric('123245'));
console.log(validator.isPostalCode('82121', 'ID'));

// chalk
console.log(chalk.blue('Hai Dunia'));
console.log(chalk.blue('Halo') + ' Dunia' + chalk.red('!'));
console.log('Kenalin aku ' + chalk.bold.green.bgWhite('Tude'));

const pesan = chalk`Halo, aku {bgGreen.red.bold siapa}. Berumur {bgBlue.black baru}.`;
console.log(pesan);